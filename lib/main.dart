import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// class for the each task. Each task has class name & title for the class.
// class is from list of class called classList.
// Initialize new task by Task('class name', 'task title')
class Task {
  String _numOfClass;
  String _taskTitle;
  Task(this._numOfClass, this._taskTitle);
}
// toDoList is a list of tasks showing in the Home page. Initialized three new tasks for demo.
List<Task> toDoList = [Task('CS421', 'HW3A'),Task('CS481', 'Lab4'),Task('CS452', 'Checkpoint2')];
// classList is a list for managing class name for Task class.
List<String> classList = ['CS421', 'CS443', 'CS452', 'CS481'];
//finishedList is a list for finished tasks which swiped right by the user from To Do List. Initialized two tasks for demo.
List<Task> finishedList = [Task('CS443', 'A7'), Task('CS421', 'ScannerProject1')];
// enum for popup choice at upper right in the Home page.
enum popupChoice {share, print, setting}


//----------------- main --------------------------------
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lab4: To Do List',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.indigo,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Home(),
    );
  }
}
//------------------ Home Page ---------------------
class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}
class _HomeState extends State<Home> {
  //show AlertDialog by showDialog function to check which button the user selected.
  popupChoice _selected;
  void _displayAlertDialog(context){
    showDialog(
        context: context,
    builder: (BuildContext context){
        return AlertDialog(
          title: Text('You selected ' + _selected.toString().split('.').last + ' button.'),
          actions: [
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('OK'),
            ),
          ],
        );
        }
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lab4: To Do List'),
        actions: [
          // Popup Menu Button from Lab3
          // I followed the sample of PopupMenuButton class
          // https://api.flutter.dev/flutter/material/PopupMenuButton-class.html
          PopupMenuButton<popupChoice>(
            onSelected: (popupChoice choice){
              setState(() {
                _selected = choice;
                _displayAlertDialog(context);
              });},
            itemBuilder: (BuildContext context) => <PopupMenuEntry<popupChoice>>[
              PopupMenuItem<popupChoice>(
                  value: popupChoice.share,
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 3.0, top: 3.0, right: 7.0, bottom: 3.0),
                        child: Icon(Icons.share,),
                      ),
                      Text('Share'),
                    ],)),
              PopupMenuItem<popupChoice>(
                  value: popupChoice.print,
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 3.0, top: 3.0, right: 7.0, bottom: 3.0),
                        child: Icon(Icons.print,),
                      ),
                      Text('Print'),
                    ],)),
              PopupMenuItem<popupChoice>(
                  value: popupChoice.setting,
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 3.0, top: 3.0, right: 7.0, bottom: 3.0),
                        child: Icon(Icons.settings,),
                      ),
                      Text('Setting'),
                    ],)),
            ],
          ),
        ], // action
      ),
      body: ToDoList(),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => Tasks()));
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.indigoAccent,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text('To Do List Menu',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 21, color: Colors.white),),
              decoration: BoxDecoration(
                color: Colors.indigo,
                ),
              ),
            ListTile(
              leading: Icon(Icons.edit),
              title: Text('Edit Class Options'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => ClassOption()));
              },
            ),
            ListTile(
              leading: Icon(Icons.check),
              title: Text('Finished Tasks'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => FinishedTask()));
              },
            ),
            ListTile(
              leading: Icon(Icons.help),
              title: Text('Help'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Guid()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
//--------------------- Help  -----------------------------
// This is Help section from the To Do List Menu.
// This page mentions some explanation about this app using Data Table from Lab3.
class Guid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.of(context).pop();
            }),
        title: Text('Help'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: [
          Text('What is this app?', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),),
          Text('This app is an app to manage the tasks you need to do today. '
                 'At the home page, you can see and manage your tasks. '
                 'This app is created for people who just want to know which school tasks need to be done today. '
                 'Thus, this app is so simple that it only can show and manage your task today.'
                 'I will explain each button and how it works below.\n',
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18),),
          Text('If you want to remove the task from your list: Done/Delete', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),),
          RichText(
            text: TextSpan(
              text: 'You can remove your task by swiping either right or left depending on the reason of removing the task from your list.\n',
              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18, ),
              children: [
                TextSpan(text: 'Swipe RIGHT ', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19),),
                TextSpan(text: ' means you get the task '),
                TextSpan(text: 'DONE', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19, color: Colors.green[600]),),
                TextSpan(text: '. The task will go to finished task list and you can see/delete the list from the Finished Tasks at To Do List Menu.\n '),
                TextSpan(text: 'Swipe LEFT ', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19),),
                TextSpan(text: ' means you '),
                TextSpan(text: 'DELETE ', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19, color: Colors.red[900]),),
                TextSpan(text: ' the task from your list permanently.\n'),
              ]
            ),
          ),
          Text('How the icon works?', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),),
          DataTable(
            columns: const [
              DataColumn(
                label: Text('Icon', style: TextStyle(fontStyle: FontStyle.italic),),
              ),
              DataColumn(
                label: Text('Work', style: TextStyle(fontStyle: FontStyle.italic),),
              ),
            ],
            rows: const [
              DataRow(
                  cells: [
                    DataCell(Icon(Icons.menu)),
                    DataCell(Text('OpenTo Do List menu.')),
                  ]
              ),
              DataRow(
                  cells: [
                    DataCell(Icon(Icons.more_vert)),
                    DataCell(Text('Open popup menu to share, print & setting.')),
                  ]
              ),
              DataRow(
                cells: [
                  DataCell(Icon(Icons.add_circle)),
                  DataCell(Text('Open the form to add new task(Home)/class(Edit Class Options).')),
                ]
              ),
              DataRow(
                  cells: [
                    DataCell(Icon(Icons.edit)),
                    DataCell(Text('Go to edit class options page.')),
                  ]
              ),
              DataRow(
                  cells: [
                    DataCell(Icon(Icons.check)),
                    DataCell(Text('Go to Finished Task page.')),
                  ]
              ),
              DataRow(
                  cells: [
                    DataCell(Icon(Icons.calendar_today)),
                    DataCell(Text('Go and check calendar.')),
                  ]
              ),
              DataRow(
                  cells: [
                    DataCell(Icon(Icons.delete)),
                    DataCell(Text('Delete the task(Finished Tasks)/class(Edit Class Options).')),
                  ]
              ),
            ],
          ),
        ],
      ),
    );
  }
}
//--------------------- Task List (Home Page) -------------------------
class ToDoList extends StatefulWidget {
  @override
  _ToDoListState createState() => _ToDoListState();
}
class _ToDoListState extends State<ToDoList> {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          child: Container(
            padding: const EdgeInsets.all(24.0),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Tasks ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32),),
                      Text('Add your task from the + button'),
                    ],
                  ),
                ),
                Text(new DateFormat.MMMEd().format(new DateTime.now()),
                  style: TextStyle(fontSize: 19),
                  textAlign: TextAlign.right,),
              ],
            ),
          ),
        ),
        Expanded(
          child: ListView.separated(
              itemCount: toDoList.length,
              separatorBuilder: (BuildContext context, int index) => Divider(),
              itemBuilder: (BuildContext context, int index){
                return Dismissible(
                    key: UniqueKey(),

                    child: Card(
                      color: Colors.black45,
                      child: ListTile(
                        contentPadding: EdgeInsets.all(8.0),
                        leading: Icon(Icons.radio_button_checked, color: Colors.indigoAccent,),
                        title: Row(
                          children: [
                            Text(toDoList[index]._numOfClass, style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),),
                            Text(':   ', style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold)),
                            Text(toDoList[index]._taskTitle, style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),),
                          ],
                        ),
                      ),
                    ),
                  onDismissed: (direction){
                      setState(() {
                        if(direction == DismissDirection.startToEnd){
                          finishedList.add(toDoList[index]);
                        }
                        toDoList.removeAt(index);
                      });},
                  background: Container(
                    color: Colors.green,
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.check_box),
                        Text('Done', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),),
                      ],
                    ),
                  ),
                  secondaryBackground: Container(
                    color: Colors.redAccent,
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.delete),
                        Text('Delete', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }
}
//-------------------- New Task Form Page ---------------------------
class Tasks extends StatefulWidget {
  @override
  _TasksState createState() => _TasksState();
}
class _TasksState extends State<Tasks> {
  String _input = '';
  String _class = '';
  int _value;
  void _displayAlertDialog(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Please fill all contents.'),
            actions: [
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('OK'),
              ),
            ],
          );
        }
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
          },
        ),
        title: Text('New Task'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 16.0, top: 16.0, right: 8.0),
        children: [
          Text('Class', style: TextStyle(fontSize: 24),
            textAlign: TextAlign.start,),
          Wrap(
            spacing: 8.0,
            runSpacing: 4.0,
            children: List<Widget>.generate(
                classList.length,
                    (int index) {
                  return ChoiceChip(
                    label: Text(classList[index]),
                    elevation: 4.0,
                    selected: _value == index,
                    selectedColor: Colors.indigoAccent,
                    onSelected: (bool selected) {
                      setState(() {
                        _value = selected ? index : null;
                        _class = classList[index];
                      });
                    },
                  );
                }
            ).toList(),
          ),
          Text('Title', style: TextStyle(fontSize: 24),
            textAlign: TextAlign.start,),
          TextField(
            maxLength: 20,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'What is your task?',
              hintText: 'eg. Lab1 report',
            ),
            onChanged: (text) {
              if (text.length > 0) {
                setState(() {
                  _input = text;
                });
              }
            },
          ),
          Center(
            child: RaisedButton(
              onPressed: () {
                if (_class.length > 0 && _input.length > 0) {
                  toDoList.add(Task(_class, _input));
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                } else {
                  _displayAlertDialog(context);
                }
              },
              child: Text('Create', style: TextStyle(fontSize: 18),),
            ),
          ),
        ],
      ),
    );
  }
}
//-----------------------  Edit Class Options Page ------------------------------
class ClassOption extends StatefulWidget {

  @override
  _ClassOptionState createState() => _ClassOptionState();
}
class _ClassOptionState extends State<ClassOption> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
          },
        ),
        title: Text('Edit Class Options'),
        centerTitle: true,
      ),
      body: GridView.count(
        crossAxisCount: 3,
        children: List.generate(
            classList.length, (index) {
          return Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0)),
            elevation: 10,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(classList[index], style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),),
                RaisedButton(
                  onPressed: (){
                    classList.removeAt(index);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ClassOption()));
                  },
                  child: Row(
                    children: [
                      Icon(Icons.delete),
                      Text('Delete'),
                    ],
                  ),
                ),
              ],
            ),
          );
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          formPopup(context);
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.indigoAccent,
      ),
    );
  }
}
Future formPopup(BuildContext context) {
  String _className = '';
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add a New Class'),
          content: TextField(
            maxLength: 10,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Enter the class name.',
            ),
            onChanged: (text){
              if(text.length > 0 ){
                _className = text;
              }
            },
          ),
          actions: [
            FlatButton(
              onPressed: (){
                Navigator.pop(context);
              },
              child: Text('Cancel'),
            ),
            FlatButton(
              onPressed: (){
                if(_className.length > 0){
                  classList.add(_className);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ClassOption()));
                } else {
                  Navigator.pop(context);
                }
              },
              child: Text('OK'),
            ),
          ],
        );
      }
  );
}
// -----------------  Display/Delete Finished Tasks Page ------------------------
class FinishedTask extends StatefulWidget {
  @override
  _FinishedTaskState createState() => _FinishedTaskState();
}
class _FinishedTaskState extends State<FinishedTask> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Home())),
        ),
        title: Text('Finished Tasks'),
        actions: [
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: (){
              setState(() {
                finishedList.clear();
              });
              Navigator.push(context, MaterialPageRoute(builder: (context) => FinishedTask()));
            },
          ),
        ],
        centerTitle: true,
      ),
      body: ListView.separated(
          itemCount: finishedList.length,
          separatorBuilder: (BuildContext context, int index) => Divider(),
          itemBuilder: (BuildContext context, int index){
            return Card(
              child: ListTile(
                contentPadding: EdgeInsets.all(8.0),
                leading: Icon(Icons.check_circle, color: Colors.green,),
                title: Row(
                  children: [
                    Text(finishedList[index]._numOfClass, style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),),
                    Text(':   ', style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold)),
                    Text(finishedList[index]._taskTitle, style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
